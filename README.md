
## Step By Step to setup SINEAS

1. Buka command line (command promt).
2. Ketik perintah dibawah ini.

## $ git clone --branch master https://zekaceka@bitbucket.org/zekaceka/sineas.git.
## $ cd sineas.
## $ yarn install.

3. koneksikan perangkat android dengan PC anda (jangan lupa untuk mengaktifkan mode USB debugging pada perangkat android anda)
4. ketik perintah di bawah ini

## $ npx react-native run-android


---

## Untuk Screenshot dan APK prototype bisa anda download melalui link dibawah ini
https://drive.google.com/drive/folders/11fJC9NQMqvDTRLBnCewdjiK_Q8cNJ3Cu?usp=sharing