import React, { Component } from 'react';
import {View, Text, StatusBar,Image,Dimensions,ScrollView,StyleSheet, Alert,FlatList,TouchableOpacity } from 'react-native';
//import { TextInput,Searchbar } from 'react-native-paper';
//import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { WebView } from 'react-native-webview';
import Moment from 'moment';

class Detail_people extends Component {
    constructor(){
        super();
        this.state = {
            similar:[],
            similar_loading:true,
            person_id:null,
            name:null,
            birthday:null,
            gender:null,
            known_for_department:null,
            place_of_birth:null,
            profile_path:null,
            biography:null,
            more_detail:false,
            shadowberanda:{shadowColor: "#FFF",shadowOffset: {width: 0,height: 0,},shadowOpacity: 0.4,shadowRadius: 10,},
        }
        Moment.locale('en');
    }
    componentDidMount(){
        if(this.state.person_id === null){
            AsyncStorage.getItem('person_id').then((person_id) => {if(person_id){
                this.setState({person_id:person_id});
                //SIMILAR
                fetch('https://api.themoviedb.org/3/person/'+person_id+'?api_key=1eaa7b1c6a8b5588fb8a4f12ba97ab06&language=en-US')
                .then((response) => response.json())
                .then((json) => {
                    this.setState({
                        name:json.name,
                        birthday:json.birthday,
                        gender:json.gender,
                        known_for_department:json.known_for_department,
                        place_of_birth:json.place_of_birth,
                        profile_path:json.profile_path,
                        biography:json.biography,
                    })
                })
                .catch((error) => console.error("Error :"+error))
                .finally(() => {});

                //SIMILAR
                // fetch('https://api.themoviedb.org/3/movie/'+movies_id+'/similar?api_key=1eaa7b1c6a8b5588fb8a4f12ba97ab06&language=en-US&page=1')
                // .then((response) => response.json())
                // .then((json) => {
                //     this.setState({
                //         similar:json.results,
                //         similar_loading:false,
                //     })
                // })
                // .catch((error) => console.error("Error :"+error))
                // .finally(() => {});
                
            }});
        }
    }

    details(id){
        AsyncStorage.setItem('movies_id', id.toString());

        this.props.navigation.replace('Detail');
    }
    render() {
        return (
            <View style={{flex: 1,backgroundColor:'#07192d',flexDirection:'column'}}>
                <StatusBar translucent backgroundColor="transparent" />
                <View style={{flex:10,flexDirection:'column'}}>
                    <View style={{flex:1,flexDirection:'row',paddingTop:5,paddingHorizontal:10}}>
                        <View style={{flex:1,justifyContent:'center'}}>
                            <TouchableOpacity onPress={()=>this.props.navigation.navigate("People")}>
                                <Icon name="arrow-left" size={25} color="#FFF" />
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:8,justifyContent:'center'}}>
                            <Text style={{color:'white',fontWeight:'bold',fontSize:20}}>People Details</Text>
                        </View>
                    </View>
                    <View style={{flex:8,flexDirection:'column'}}>
                        <ScrollView>
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:1,paddingHorizontal:10}}>
                                    <Image source={{uri:'https://image.tmdb.org/t/p/w500'+this.state.profile_path}} 
                                    style={{width:'100%',height:150,borderRadius:10}} />
                                </View>
                                <View style={{flex:2}}>
                                    <Text style={{color:'white',fontWeight:'bold',fontSize:17}}>Personal info</Text>
                                    <View style={{flexDirection:'row'}}>
                                        <View style={{flex:1}}>
                                            <Text style={{color:'white',fontWeight:'bold',fontSize:12}}>Name</Text>
                                            <Text style={{color:'white',fontSize:10,textAlign:'justify'}}>{this.state.name}</Text>
                                        </View>
                                        <View style={{flex:1}}>
                                            <Text style={{color:'white',fontWeight:'bold',fontSize:12}}>Gender</Text>
                                            {this.state.gender === 1?(
                                                <Text style={{color:'white',fontSize:10,textAlign:'justify'}}>Female</Text>
                                            ):(
                                                <Text style={{color:'white',fontSize:10,textAlign:'justify'}}>Male</Text>
                                            )}
                                        </View>
                                    </View>
                                    <View style={{flexDirection:'row',marginTop:10}}>
                                        <View style={{flex:1}}>
                                            <Text style={{color:'white',fontWeight:'bold',fontSize:12}}>Place of Birth</Text>
                                            <Text style={{color:'white',fontSize:10,textAlign:'justify'}}>{this.state.place_of_birth}</Text>
                                        </View>
                                        <View style={{flex:1}}>
                                            <Text style={{color:'white',fontWeight:'bold',fontSize:12}}>Birthday</Text>
                                            <Text style={{color:'white',fontSize:10,textAlign:'justify'}}>{this.state.birthday}</Text>
                                        </View>
                                    </View>
                                    <View style={{flexDirection:'row',marginTop:10}}>
                                        <View style={{flex:1}}>
                                            <Text style={{color:'white',fontWeight:'bold',fontSize:12}}>Known For</Text>
                                            <Text style={{color:'white',fontSize:10,textAlign:'justify'}}>{this.state.known_for_department}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={{paddingHorizontal:10}}>
                                <Text style={{color:'white',fontWeight:'bold',fontSize:17}}>Biography</Text>
                                <Text style={{color:'white',fontSize:11,textAlign:'justify'}}>{this.state.biography}</Text>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <View style={{flex:1,flexDirection:'row',borderTopWidth:0.3,borderTopColor:'#CCC'}}>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Home')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="video-camera" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                Movies
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Tv')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="tv" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                TV Shows
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Bookmark')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="bookmark" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                Watch list
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('People')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="user" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                People
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

export default Detail_people;