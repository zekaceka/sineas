import React, { Component } from 'react';
import {View, Text, StatusBar,Image,Dimensions,ScrollView,StyleSheet, Alert,FlatList,TouchableOpacity } from 'react-native';
//import { TextInput,Searchbar } from 'react-native-paper';
//import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { WebView } from 'react-native-webview';
import Moment from 'moment';

class People extends Component {
    constructor(){
        super();
        this.state = {
            people:[],
            people_loading:true,
            movies_id:null,
            title:null,
            release_date:null,
            backdrop_path:null,
            overview:null,
            youtube_id:'pBVPlyM2T3Q',
            play:true,
            shadowberanda:{shadowColor: "#FFF",shadowOffset: {width: 0,height: 0,},shadowOpacity: 0.4,shadowRadius: 10,},
        }
        Moment.locale('en');
        //PEOPLE POPULAR
        fetch('https://api.themoviedb.org/3/person/popular?api_key=1eaa7b1c6a8b5588fb8a4f12ba97ab06&language=en-US&page=1')
        .then((response) => response.json())
        .then((json) => {
            this.setState({
                people:json.results,
                people_loading:false,
            })
        })
        .catch((error) => console.error("Error :"+error))
        .finally(() => {});
    }
    componentDidMount(){
        if(this.state.movies_id === null){
            AsyncStorage.getItem('movies_id').then((movies_id) => {if(movies_id){
                
            }});
        }
    }

    details(id){
        AsyncStorage.setItem('person_id', id.toString());

        this.props.navigation.replace('Detail_people');
    }
    render() {
        return (
            <View style={{flex: 1,backgroundColor:'#07192d',flexDirection:'column'}}>
                <StatusBar translucent backgroundColor="transparent" />
                <View style={{flex:10,flexDirection:'column'}}>
                    <View style={{flex:1,flexDirection:'row',paddingTop:5,paddingHorizontal:15}}>
                        <View style={{flex:1,justifyContent:'center'}}>
                            <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                                <Icon name="arrow-left" size={25} color="#FFF" />
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:8,justifyContent:'center'}}>
                            <Text style={{color:'white',fontWeight:'bold',fontSize:20}}>People Popular</Text>
                        </View>
                    </View>
                    <View style={{flex:8,flexDirection:'column'}}>
                        <ScrollView>
                            <View style={{paddingHorizontal:15}}>
                                <View style={{flexDirection:'row',marginBottom:10}}>
                                    {this.state.people_loading === true ?(
                                        <ScrollView horizontal>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                        </ScrollView>
                                    ):(
                                        <ScrollView horizontal>
                                            <FlatList
                                            numColumns={3}
                                            data = {this.state.people}
                                            renderItem={({item})=>(
                                                <TouchableOpacity onPress={()=>this.details(item.id)}
                                                style={{paddingRight:5,marginBottom:10,width:(Dimensions.get('window').width/3)-8}}>
                                                    <Image source={{uri:'https://image.tmdb.org/t/p/w500'+item.profile_path}} 
                                                    style={{width:'100%',height:150,borderRadius:5}} />
                                                    <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{item.name}</Text>
                                                    {item.gender === 1 ?(
                                                        <Text style={{color:'#CCC',fontSize:11}}>Female</Text>
                                                    ):(
                                                        <Text style={{color:'#CCC',fontSize:11}}>Male</Text>
                                                    )}
                                                </TouchableOpacity>
                                            )}
                                            />
                                        </ScrollView>
                                    )}
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <View style={{flex:1,flexDirection:'row',borderTopWidth:0.3,borderTopColor:'#CCC'}}>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Home')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="video-camera" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                Movies
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Tv')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="tv" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                TV Shows
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Bookmark')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="bookmark" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                Watch list
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('People')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="user" size={25} color="#dc2a4e" />
                            <Text style={{color:'#dc2a4e',fontSize:9}}>
                                People
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

export default People;