import React, {useEffect,useState} from 'react';
import {View,Text,Image,StatusBar} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LinearGradient from 'react-native-linear-gradient';
    const Splash = ({navigation}) => {
    useEffect(() => {
        //setTimeout(() => {
          // navigation.replace("Login");
          cek_session();
        //}, 3000);
    });

    const cek_session = () => {
        setTimeout(() => {
            navigation.replace('Home');
        }, 2000);
  }
    return(
      <View style={{ flex: 1,backgroundColor:'#07192d',justifyContent:'center',alignItems:'center'}}>
        <StatusBar translucent backgroundColor="transparent" />
        <Text style={{color:'#dc2a4e',fontSize:40,fontWeight:'bold'}}>
            S I N E A S
        </Text>
        <Text style={{color:'white',fontSize:12}}>
            SINEMA ANAK BANGSA
        </Text>
      </View>
    )
}
export default Splash;