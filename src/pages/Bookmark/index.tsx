import React, { Component } from 'react';
import {View, Text, StatusBar,Image,Dimensions,ScrollView,StyleSheet, Alert,FlatList,TouchableOpacity } from 'react-native';
//import { TextInput,Searchbar } from 'react-native-paper';
//import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { WebView } from 'react-native-webview';
import Moment from 'moment';

class Bookmark extends Component {
    constructor(){
        super();
        this.state = {
            people:[],
            people_loading:true,
            movies_id:null,
            title:null,
            release_date:null,
            backdrop_path:null,
            overview:null,
            youtube_id:'pBVPlyM2T3Q',
            play:true,
            movies_id1:null,title1:'',poster_path1:'',release_date1:'',
            movies_id2:null,title2:'',poster_path2:'',release_date2:'',
            movies_id3:null,title3:'',poster_path3:'',release_date3:'',
            movies_id4:null,title4:'',poster_path4:'',release_date4:'',
            movies_id5:null,title5:'',poster_path5:'',release_date5:'',
            movies_id6:null,title6:'',poster_path6:'',release_date6:'',
            movies_id7:null,title7:'',poster_path7:'',release_date7:'',
            movies_id8:null,title8:'',poster_path8:'',release_date8:'',
            movies_id9:null,title9:'',poster_path9:'',release_date9:'',
            movies_id10:null,title10:'',poster_path10:'',release_date10:'',
            movies_id11:null,title11:'',poster_path11:'',release_date11:'',
            movies_id12:null,title12:'',poster_path12:'',release_date12:'',
            movies_id13:null,title13:'',poster_path13:'',release_date13:'',
            movies_id14:null,title14:'',poster_path14:'',release_date14:'',
            movies_id15:null,title15:'',poster_path15:'',release_date15:'',
            movies_id16:null,title16:'',poster_path16:'',release_date16:'',
            movies_id17:null,title17:'',poster_path17:'',release_date17:'',
            movies_id18:null,title18:'',poster_path18:'',release_date18:'',
            movies_id19:null,title19:'',poster_path19:'',release_date19:'',
            movies_id20:null,title20:'',poster_path20:'',release_date20:'',
            shadowberanda:{shadowColor: "#FFF",shadowOffset: {width: 0,height: 0,},shadowOpacity: 0.4,shadowRadius: 10,},
        }
        Moment.locale('en');
        this.load_data();
        // AsyncStorage.multiRemove([
        //     'movies_id1','title1','poster_path1','release_date1',
        //     'movies_id2','title2','poster_path2','release_date2',
        //     'movies_id3','title3','poster_path3','release_date3',
        //     'movies_id4','title4','poster_path4','release_date4',
        //     'movies_id5','title5','poster_path5','release_date5',
        //     'movies_id6','title6','poster_path6','release_date6',
        //     'movies_id7','title7','poster_path7','release_date7',
        //     'movies_id8','title8','poster_path8','release_date8',
        //     'movies_id9','title9','poster_path9','release_date9'
        // ]);
    }
    load_data(){
        AsyncStorage.getItem('movies_id1').then((movies_id) => {if(movies_id){this.setState({movies_id1:movies_id})}});
        AsyncStorage.getItem('movies_id2').then((movies_id) => {if(movies_id){this.setState({movies_id2:movies_id})}});
        AsyncStorage.getItem('movies_id3').then((movies_id) => {if(movies_id){this.setState({movies_id3:movies_id})}});
        AsyncStorage.getItem('movies_id4').then((movies_id) => {if(movies_id){this.setState({movies_id4:movies_id})}});
        AsyncStorage.getItem('movies_id5').then((movies_id) => {if(movies_id){this.setState({movies_id5:movies_id})}});
        AsyncStorage.getItem('movies_id6').then((movies_id) => {if(movies_id){this.setState({movies_id6:movies_id})}});
        AsyncStorage.getItem('movies_id7').then((movies_id) => {if(movies_id){this.setState({movies_id7:movies_id})}});
        AsyncStorage.getItem('movies_id8').then((movies_id) => {if(movies_id){this.setState({movies_id8:movies_id})}});
        AsyncStorage.getItem('movies_id9').then((movies_id) => {if(movies_id){this.setState({movies_id9:movies_id})}});
        AsyncStorage.getItem('movies_id10').then((movies_id) => {if(movies_id){this.setState({movies_id10:movies_id})}});
        AsyncStorage.getItem('movies_id11').then((movies_id) => {if(movies_id){this.setState({movies_id11:movies_id})}});
        AsyncStorage.getItem('movies_id12').then((movies_id) => {if(movies_id){this.setState({movies_id12:movies_id})}});
        AsyncStorage.getItem('movies_id13').then((movies_id) => {if(movies_id){this.setState({movies_id13:movies_id})}});
        AsyncStorage.getItem('movies_id14').then((movies_id) => {if(movies_id){this.setState({movies_id14:movies_id})}});
        AsyncStorage.getItem('movies_id15').then((movies_id) => {if(movies_id){this.setState({movies_id15:movies_id})}});
        AsyncStorage.getItem('movies_id16').then((movies_id) => {if(movies_id){this.setState({movies_id16:movies_id})}});
        AsyncStorage.getItem('movies_id17').then((movies_id) => {if(movies_id){this.setState({movies_id17:movies_id})}});
        AsyncStorage.getItem('movies_id18').then((movies_id) => {if(movies_id){this.setState({movies_id18:movies_id})}});
        AsyncStorage.getItem('movies_id19').then((movies_id) => {if(movies_id){this.setState({movies_id19:movies_id})}});
        AsyncStorage.getItem('movies_id20').then((movies_id) => {if(movies_id){this.setState({movies_id20:movies_id})}});

        AsyncStorage.getItem('title1').then((title) => {if(title){this.setState({title1:title})}});
        AsyncStorage.getItem('title2').then((title) => {if(title){this.setState({title2:title})}});
        AsyncStorage.getItem('title3').then((title) => {if(title){this.setState({title3:title})}});
        AsyncStorage.getItem('title4').then((title) => {if(title){this.setState({title4:title})}});
        AsyncStorage.getItem('title5').then((title) => {if(title){this.setState({title5:title})}});
        AsyncStorage.getItem('title6').then((title) => {if(title){this.setState({title6:title})}});
        AsyncStorage.getItem('title7').then((title) => {if(title){this.setState({title7:title})}});
        AsyncStorage.getItem('title8').then((title) => {if(title){this.setState({title8:title})}});
        AsyncStorage.getItem('title9').then((title) => {if(title){this.setState({title9:title})}});
        AsyncStorage.getItem('title10').then((title) => {if(title){this.setState({title10:title})}});
        AsyncStorage.getItem('title11').then((title) => {if(title){this.setState({title11:title})}});
        AsyncStorage.getItem('title12').then((title) => {if(title){this.setState({title12:title})}});
        AsyncStorage.getItem('title13').then((title) => {if(title){this.setState({title13:title})}});
        AsyncStorage.getItem('title14').then((title) => {if(title){this.setState({title14:title})}});
        AsyncStorage.getItem('title15').then((title) => {if(title){this.setState({title15:title})}});
        AsyncStorage.getItem('title16').then((title) => {if(title){this.setState({title16:title})}});
        AsyncStorage.getItem('title17').then((title) => {if(title){this.setState({title17:title})}});
        AsyncStorage.getItem('title18').then((title) => {if(title){this.setState({title18:title})}});
        AsyncStorage.getItem('title19').then((title) => {if(title){this.setState({title19:title})}});
        AsyncStorage.getItem('title20').then((title) => {if(title){this.setState({title20:title})}});

        AsyncStorage.getItem('poster_path1').then((poster_path) => {if(poster_path){this.setState({poster_path1:poster_path})}});
        AsyncStorage.getItem('poster_path2').then((poster_path) => {if(poster_path){this.setState({poster_path2:poster_path})}});
        AsyncStorage.getItem('poster_path3').then((poster_path) => {if(poster_path){this.setState({poster_path3:poster_path})}});
        AsyncStorage.getItem('poster_path4').then((poster_path) => {if(poster_path){this.setState({poster_path4:poster_path})}});
        AsyncStorage.getItem('poster_path5').then((poster_path) => {if(poster_path){this.setState({poster_path5:poster_path})}});
        AsyncStorage.getItem('poster_path6').then((poster_path) => {if(poster_path){this.setState({poster_path6:poster_path})}});
        AsyncStorage.getItem('poster_path7').then((poster_path) => {if(poster_path){this.setState({poster_path7:poster_path})}});
        AsyncStorage.getItem('poster_path8').then((poster_path) => {if(poster_path){this.setState({poster_path8:poster_path})}});
        AsyncStorage.getItem('poster_path9').then((poster_path) => {if(poster_path){this.setState({poster_path9:poster_path})}});
        AsyncStorage.getItem('poster_path10').then((poster_path) => {if(poster_path){this.setState({poster_path10:poster_path})}});
        AsyncStorage.getItem('poster_path11').then((poster_path) => {if(poster_path){this.setState({poster_path11:poster_path})}});
        AsyncStorage.getItem('poster_path12').then((poster_path) => {if(poster_path){this.setState({poster_path12:poster_path})}});
        AsyncStorage.getItem('poster_path13').then((poster_path) => {if(poster_path){this.setState({poster_path13:poster_path})}});
        AsyncStorage.getItem('poster_path14').then((poster_path) => {if(poster_path){this.setState({poster_path14:poster_path})}});
        AsyncStorage.getItem('poster_path15').then((poster_path) => {if(poster_path){this.setState({poster_path15:poster_path})}});
        AsyncStorage.getItem('poster_path16').then((poster_path) => {if(poster_path){this.setState({poster_path16:poster_path})}});
        AsyncStorage.getItem('poster_path17').then((poster_path) => {if(poster_path){this.setState({poster_path17:poster_path})}});
        AsyncStorage.getItem('poster_path18').then((poster_path) => {if(poster_path){this.setState({poster_path18:poster_path})}});
        AsyncStorage.getItem('poster_path19').then((poster_path) => {if(poster_path){this.setState({poster_path19:poster_path})}});
        AsyncStorage.getItem('poster_path20').then((poster_path) => {if(poster_path){this.setState({poster_path20:poster_path})}});
        
        AsyncStorage.getItem('release_date1').then((release_date) => {if(release_date){this.setState({release_date1:release_date})}});
        AsyncStorage.getItem('release_date2').then((release_date) => {if(release_date){this.setState({release_date2:release_date})}});
        AsyncStorage.getItem('release_date3').then((release_date) => {if(release_date){this.setState({release_date3:release_date})}});
        AsyncStorage.getItem('release_date4').then((release_date) => {if(release_date){this.setState({release_date4:release_date})}});
        AsyncStorage.getItem('release_date5').then((release_date) => {if(release_date){this.setState({release_date5:release_date})}});
        AsyncStorage.getItem('release_date6').then((release_date) => {if(release_date){this.setState({release_date6:release_date})}});
        AsyncStorage.getItem('release_date7').then((release_date) => {if(release_date){this.setState({release_date7:release_date})}});
        AsyncStorage.getItem('release_date8').then((release_date) => {if(release_date){this.setState({release_date8:release_date})}});
        AsyncStorage.getItem('release_date9').then((release_date) => {if(release_date){this.setState({release_date9:release_date})}});
        AsyncStorage.getItem('release_date10').then((release_date) => {if(release_date){this.setState({release_date10:release_date})}});
        AsyncStorage.getItem('release_date11').then((release_date) => {if(release_date){this.setState({release_date11:release_date})}});
        AsyncStorage.getItem('release_date12').then((release_date) => {if(release_date){this.setState({release_date12:release_date})}});
        AsyncStorage.getItem('release_date13').then((release_date) => {if(release_date){this.setState({release_date13:release_date})}});
        AsyncStorage.getItem('release_date14').then((release_date) => {if(release_date){this.setState({release_date14:release_date})}});
        AsyncStorage.getItem('release_date15').then((release_date) => {if(release_date){this.setState({release_date15:release_date})}});
        AsyncStorage.getItem('release_date16').then((release_date) => {if(release_date){this.setState({release_date16:release_date})}});
        AsyncStorage.getItem('release_date17').then((release_date) => {if(release_date){this.setState({release_date17:release_date})}});
        AsyncStorage.getItem('release_date18').then((release_date) => {if(release_date){this.setState({release_date18:release_date})}});
        AsyncStorage.getItem('release_date19').then((release_date) => {if(release_date){this.setState({release_date19:release_date})}});
        AsyncStorage.getItem('release_date20').then((release_date) => {if(release_date){this.setState({release_date20:release_date})}});
    }
    componentDidMount(){
        if(this.state.movies_id === null){
            AsyncStorage.getItem('movies_id').then((movies_id) => {if(movies_id){
                
            }});
        }
    }

    details(id){
        AsyncStorage.setItem('movies_id', id.toString());

        this.props.navigation.replace('Detail');
    }
    render() {
        return (
            <View style={{flex: 1,backgroundColor:'#07192d',flexDirection:'column'}}>
                <StatusBar translucent backgroundColor="transparent" />
                <View style={{flex:10,flexDirection:'column'}}>
                    <View style={{flex:1,flexDirection:'row',paddingTop:5,paddingHorizontal:15}}>
                        <View style={{flex:1,justifyContent:'center'}}>
                            <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                                <Icon name="arrow-left" size={25} color="#FFF" />
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:8,justifyContent:'center'}}>
                            <Text style={{color:'white',fontWeight:'bold',fontSize:20}}>Watch list</Text>
                        </View>
                    </View>
                    <View style={{flex:8,flexDirection:'column'}}>
                        <ScrollView>
                            <View style={{paddingHorizontal:15}}>
                                <View style={{flexDirection:'row',marginBottom:10}}>
                                    {this.state.movies_id1 !== null &&(
                                        <TouchableOpacity onPress={()=>this.details(this.state.movies_id1)}
                                        style={{paddingRight:5,marginBottom:10,width:(Dimensions.get('window').width/3)-8}}>
                                            <Image source={{uri:'https://image.tmdb.org/t/p/w500'+this.state.poster_path1}} 
                                            style={{width:'100%',height:150,borderRadius:5}} />
                                            <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{this.state.title1}</Text>
                                            <Text style={{color:'#CCC',fontSize:11}}>{this.state.release_date1}</Text>
                                        </TouchableOpacity>
                                    )}
                                    {this.state.movies_id2 !== null &&(
                                        <TouchableOpacity onPress={()=>this.details(this.state.movies_id2)}
                                        style={{paddingRight:5,marginBottom:10,width:(Dimensions.get('window').width/3)-8}}>
                                            <Image source={{uri:'https://image.tmdb.org/t/p/w500'+this.state.poster_path2}} 
                                            style={{width:'100%',height:150,borderRadius:5}} />
                                            <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{this.state.title2}</Text>
                                            <Text style={{color:'#CCC',fontSize:11}}>{this.state.release_date2}</Text>
                                        </TouchableOpacity>
                                    )}
                                    {this.state.movies_id3 !== null &&(
                                        <TouchableOpacity onPress={()=>this.details(this.state.movies_id3)}
                                        style={{paddingRight:5,marginBottom:10,width:(Dimensions.get('window').width/3)-8}}>
                                            <Image source={{uri:'https://image.tmdb.org/t/p/w500'+this.state.poster_path3}} 
                                            style={{width:'100%',height:150,borderRadius:5}} />
                                            <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{this.state.title3}</Text>
                                            <Text style={{color:'#CCC',fontSize:11}}>{this.state.release_date3}</Text>
                                        </TouchableOpacity>
                                    )}
                                </View>
                                <View style={{flexDirection:'row',marginBottom:10}}>
                                    {this.state.movies_id4 !== null &&(
                                        <TouchableOpacity onPress={()=>this.details(this.state.movies_id4)}
                                        style={{paddingRight:5,marginBottom:10,width:(Dimensions.get('window').width/3)-8}}>
                                            <Image source={{uri:'https://image.tmdb.org/t/p/w500'+this.state.poster_path4}} 
                                            style={{width:'100%',height:150,borderRadius:5}} />
                                            <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{this.state.title4}</Text>
                                            <Text style={{color:'#CCC',fontSize:11}}>{this.state.release_date4}</Text>
                                        </TouchableOpacity>
                                    )}
                                    {this.state.movies_id5 !== null &&(
                                        <TouchableOpacity onPress={()=>this.details(this.state.movies_id5)}
                                        style={{paddingRight:5,marginBottom:10,width:(Dimensions.get('window').width/3)-8}}>
                                            <Image source={{uri:'https://image.tmdb.org/t/p/w500'+this.state.poster_path5}} 
                                            style={{width:'100%',height:150,borderRadius:5}} />
                                            <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{this.state.title5}</Text>
                                            <Text style={{color:'#CCC',fontSize:11}}>{this.state.release_date5}</Text>
                                        </TouchableOpacity>
                                    )}
                                    {this.state.movies_id6 !== null &&(
                                        <TouchableOpacity onPress={()=>this.details(this.state.movies_id6)}
                                        style={{paddingRight:5,marginBottom:10,width:(Dimensions.get('window').width/3)-8}}>
                                            <Image source={{uri:'https://image.tmdb.org/t/p/w500'+this.state.poster_path6}} 
                                            style={{width:'100%',height:150,borderRadius:5}} />
                                            <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{this.state.title6}</Text>
                                            <Text style={{color:'#CCC',fontSize:11}}>{this.state.release_date6}</Text>
                                        </TouchableOpacity>
                                    )}
                                </View>
                                <View style={{flexDirection:'row',marginBottom:10}}>
                                    {this.state.movies_id7 !== null &&(
                                        <TouchableOpacity onPress={()=>this.details(this.state.movies_id7)}
                                        style={{paddingRight:5,marginBottom:10,width:(Dimensions.get('window').width/3)-8}}>
                                            <Image source={{uri:'https://image.tmdb.org/t/p/w500'+this.state.poster_path7}} 
                                            style={{width:'100%',height:150,borderRadius:5}} />
                                            <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{this.state.title7}</Text>
                                            <Text style={{color:'#CCC',fontSize:11}}>{this.state.release_date7}</Text>
                                        </TouchableOpacity>
                                    )}
                                    {this.state.movies_id8 !== null &&(
                                        <TouchableOpacity onPress={()=>this.details(this.state.movies_id8)}
                                        style={{paddingRight:5,marginBottom:10,width:(Dimensions.get('window').width/3)-8}}>
                                            <Image source={{uri:'https://image.tmdb.org/t/p/w500'+this.state.poster_path8}} 
                                            style={{width:'100%',height:150,borderRadius:5}} />
                                            <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{this.state.title8}</Text>
                                            <Text style={{color:'#CCC',fontSize:11}}>{this.state.release_date8}</Text>
                                        </TouchableOpacity>
                                    )}
                                    {this.state.movies_id9 !== null &&(
                                        <TouchableOpacity onPress={()=>this.details(this.state.movies_id9)}
                                        style={{paddingRight:5,marginBottom:10,width:(Dimensions.get('window').width/3)-8}}>
                                            <Image source={{uri:'https://image.tmdb.org/t/p/w500'+this.state.poster_path9}} 
                                            style={{width:'100%',height:150,borderRadius:5}} />
                                            <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{this.state.title9}</Text>
                                            <Text style={{color:'#CCC',fontSize:11}}>{this.state.release_date9}</Text>
                                        </TouchableOpacity>
                                    )}
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <View style={{flex:1,flexDirection:'row',borderTopWidth:0.3,borderTopColor:'#CCC'}}>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Home')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="video-camera" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                Movies
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Tv')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="tv" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                TV Shows
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Bookmark')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="bookmark" size={25} color="#dc2a4e" />
                            <Text style={{color:'#dc2a4e',fontSize:9}}>
                                Watch list
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('People')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="user" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                People
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

export default Bookmark;