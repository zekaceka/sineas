import React, { Component } from 'react';
import {View, Text, StatusBar,Image,Dimensions,ScrollView,StyleSheet, Alert,FlatList,TouchableOpacity } from 'react-native';
//import { TextInput,Searchbar } from 'react-native-paper';
//import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { WebView } from 'react-native-webview';
import Moment from 'moment';

class Detail extends Component {
    constructor(){
        super();
        this.state = {
            similar:[],
            similar_loading:true,
            movies_id:null,
            title:null,
            release_date:null,
            backdrop_path:null,
            overview:null,
            youtube_id:'pBVPlyM2T3Q',
            play:true,
            shadowberanda:{shadowColor: "#FFF",shadowOffset: {width: 0,height: 0,},shadowOpacity: 0.4,shadowRadius: 10,},
        }
        Moment.locale('en');
    }
    componentDidMount(){
        if(this.state.movies_id === null){
            AsyncStorage.getItem('movies_id').then((movies_id) => {if(movies_id){
                this.setState({movies_id:movies_id});
                //SIMILAR
                fetch('https://api.themoviedb.org/3/movie/'+movies_id+'?api_key=1eaa7b1c6a8b5588fb8a4f12ba97ab06&language=en-US')
                .then((response) => response.json())
                .then((json) => {
                    this.setState({
                        title:json.title,
                        backdrop_path:json.backdrop_path,
                        poster_path:json.poster_path,
                        overview:json.overview,
                        release_date:Moment(json.release_date).format('YYYY'),
                    })
                })
                .catch((error) => console.error("Error :"+error))
                .finally(() => {});

                //SIMILAR
                fetch('https://api.themoviedb.org/3/movie/'+movies_id+'/similar?api_key=1eaa7b1c6a8b5588fb8a4f12ba97ab06&language=en-US&page=1')
                .then((response) => response.json())
                .then((json) => {
                    this.setState({
                        similar:json.results,
                        similar_loading:false,
                    })
                })
                .catch((error) => console.error("Error :"+error))
                .finally(() => {});
                
                fetch('https://api.themoviedb.org/3/movie/'+movies_id+'/videos?api_key=1eaa7b1c6a8b5588fb8a4f12ba97ab06&language=en-US')
                .then((response) => response.json())
                .then((json) => {
                    const data = JSON.stringify(json.results);
                    JSON.parse(data, (key, value) => {
                        //console.warn(value);
                        if(key == 0){

                        }else if(key == "key"){
                            this.setState({
                                youtube_id:value
                            });
                        }
                    });
                })
                .catch((error) => console.error("Error :"+error))
                .finally(() => {});
            }});
        }
    }

    details(id){
        AsyncStorage.setItem('movies_id', id.toString());

        this.props.navigation.replace('Detail');
    }
    bookmark(movies_id,poster_path,title,release_date){
        AsyncStorage.getItem('movies_id1').then((id) => {
            if(id){
                AsyncStorage.getItem('movies_id2').then((id) => {
                    if(id){
                        AsyncStorage.getItem('movies_id3').then((id) => {
                            if(id){
                                AsyncStorage.getItem('movies_id4').then((id) => {
                                    if(id){
                                        AsyncStorage.getItem('movies_id5').then((id) => {
                                            if(id){
                                                AsyncStorage.getItem('movies_id6').then((id) => {
                                                    if(id){
                                                        AsyncStorage.getItem('movies_id7').then((id) => {
                                                            if(id){
                                                                AsyncStorage.getItem('movies_id8').then((id) => {
                                                                    if(id){
                                                                        AsyncStorage.getItem('movies_id9').then((id) => {
                                                                            if(id){
                                                                
                                                                            }else{
                                                                                AsyncStorage.setItem('movies_id9', movies_id.toString());
                                                                                AsyncStorage.setItem('poster_path9', poster_path);
                                                                                AsyncStorage.setItem('title9', title);
                                                                                AsyncStorage.setItem('release_date9', release_date);
                                                                            }
                                                                        });
                                                                    }else{
                                                                        AsyncStorage.setItem('movies_id8', movies_id.toString());
                                                                        AsyncStorage.setItem('poster_path8', poster_path);
                                                                        AsyncStorage.setItem('title8', title);
                                                                        AsyncStorage.setItem('release_date8', release_date);
                                                                    }
                                                                });
                                                            }else{
                                                                AsyncStorage.setItem('movies_id7', movies_id.toString());
                                                                AsyncStorage.setItem('poster_path7', poster_path);
                                                                AsyncStorage.setItem('title7', title);
                                                                AsyncStorage.setItem('release_date7', release_date);
                                                            }
                                                        });
                                                    }else{
                                                        AsyncStorage.setItem('movies_id6', movies_id.toString());
                                                        AsyncStorage.setItem('poster_path6', poster_path);
                                                        AsyncStorage.setItem('title6', title);
                                                        AsyncStorage.setItem('release_date6', release_date);
                                                    }
                                                });
                                            }else{
                                                AsyncStorage.setItem('movies_id5', movies_id.toString());
                                                AsyncStorage.setItem('poster_path5', poster_path);
                                                AsyncStorage.setItem('title5', title);
                                                AsyncStorage.setItem('release_date5', release_date);
                                            }
                                        });
                                    }else{
                                        AsyncStorage.setItem('movies_id4', movies_id.toString());
                                        AsyncStorage.setItem('poster_path4', poster_path);
                                        AsyncStorage.setItem('title4', title);
                                        AsyncStorage.setItem('release_date4', release_date);
                                    }
                                });
                            }else{
                                AsyncStorage.setItem('movies_id3', movies_id.toString());
                                AsyncStorage.setItem('poster_path3', poster_path);
                                AsyncStorage.setItem('title3', title);
                                AsyncStorage.setItem('release_date3', release_date);
                            }
                        });
                    }else{
                        AsyncStorage.setItem('movies_id2', movies_id.toString());
                        AsyncStorage.setItem('poster_path2', poster_path);
                        AsyncStorage.setItem('title2', title);
                        AsyncStorage.setItem('release_date2', release_date);
                    }
                });
            }else{
                AsyncStorage.setItem('movies_id1', movies_id.toString());
                AsyncStorage.setItem('poster_path1', poster_path);
                AsyncStorage.setItem('title1', title);
                AsyncStorage.setItem('release_date1', release_date);
            }
        });
    }
    render() {
        return (
            <View style={{flex: 1,backgroundColor:'#07192d',flexDirection:'column'}}>
                <StatusBar translucent backgroundColor="transparent" />
                <View style={{flex:10,flexDirection:'column'}}>
                    
                    <View style={{flex:9,flexDirection:'column'}}>
                        <ScrollView>
                            <View>
                                {this.state.play === false ? (
                                    <WebView
                                        source={{ uri: 'https://www.youtube.com/embed/'+this.state.youtube_id }}
                                        style={{ width:'100%',height:240,marginTop:60 }}
                                    />
                                ):(
                                    <Image source={{uri:'https://image.tmdb.org/t/p/w500'+this.state.backdrop_path}} 
                                    style={{width:'100%',height:300}} />
                                )}
                                {this.state.play === true && (
                                    <View style={{width:'100%',height:300,position:'absolute',elevation:50,justifyContent:'center',alignItems:'center'}}>
                                        <TouchableOpacity onPress={()=>this.setState({play:false})} style={{borderWidth:1,borderColor:'#FFF',borderRadius:25,width:50,height:50,justifyContent:'center',alignItems:'center',paddingLeft:5}}>
                                                <Icon name="play" size={25} color="#FFF" />
                                        </TouchableOpacity>
                                    </View>
                                )}
                                <View style={{flexDirection:'row',position:'absolute',top:30,elevation:100,paddingHorizontal:10}}>
                                    <View style={{flex:1,justifyContent:'center'}}>
                                        <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                                            <Icon name="arrow-left" size={25} color="#FFF" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flex:8,justifyContent:'center'}}>
                                        <Text style={{color:'white',fontWeight:'bold',fontSize:20}}>Details movies</Text>
                                    </View>
                                    <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
                                        <TouchableOpacity onPress={()=>this.bookmark(this.state.movies_id,this.state.poster_path,this.state.title,this.state.release_date)}>
                                            <Icon name="bookmark" size={25} color="#FFF" />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{marginTop:10,paddingHorizontal:15}}>
                                    <Text style={{color:'white',fontWeight:'bold',fontSize:17}}>{this.state.title} ( {this.state.release_date} )</Text>
                                    <Text style={{color:'white',fontSize:11,textAlign:'justify'}}>{this.state.overview}</Text>
                                </View>
                            </View>
                            <View style={{paddingHorizontal:15}}>
                                <View style={{marginTop:10}}>
                                    <Text style={{color:'white',fontWeight:'bold'}}>SIMILAR MOVIES</Text>
                                </View>
                                <View style={{marginTop:10,flexDirection:'row',marginBottom:10}}>
                                    {this.state.similar_loading === true ?(
                                        <ScrollView horizontal>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                        </ScrollView>
                                    ):(
                                        <ScrollView horizontal>
                                            <FlatList
                                            numColumns={100}
                                            data = {this.state.similar}
                                            renderItem={({item})=>(
                                                <TouchableOpacity onPress={()=>this.details(item.id)}
                                                style={{marginRight:5,width:100}}>
                                                    <Image source={{uri:'https://image.tmdb.org/t/p/w500'+item.poster_path}} 
                                                    style={{width:100,height:150,borderRadius:5}} />
                                                    <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{item.title}</Text>
                                                    <Text style={{color:'#CCC',fontSize:11}}>{Moment(item.release_date).format('YYYY')}</Text>
                                                </TouchableOpacity>
                                            )}
                                            />
                                        </ScrollView>
                                    )}
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <View style={{flex:1,flexDirection:'row',borderTopWidth:0.3,borderTopColor:'#CCC'}}>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Home')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="video-camera" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                Movies
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Tv')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="tv" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                TV Shows
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Bookmark')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="bookmark" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                Watch list
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('People')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="user" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                People
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

export default Detail;