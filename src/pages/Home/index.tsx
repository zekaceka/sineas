import React, { Component } from 'react';
import {View, Text, StatusBar,Image,Dimensions,ScrollView,StyleSheet, Alert,FlatList } from 'react-native';
import { TextInput,Searchbar } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import DropShadow from "react-native-drop-shadow";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Carousel from 'react-native-snap-carousel';
import BlinkView from 'react-native-blink-view'
import { scrollInterpolator, animatedStyles } from '../Utils';
import Moment from 'moment';

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 1.2);

const DATA0 = [];
const DATA = [];
for (let i = 0; i < 4; i++) {
    DATA0.push(i)
}

class Home extends Component {
    constructor(){
        super();
        this.state = {
            index: 0,
            index0: 0,
            test: "",
            upcoming:[],
            toprated:[],
            popular:[],
            now_loading:true,
            upcoming_loading:true,
            toprated_loading:true,
            popular_loading:true,
            shadowberanda:{shadowColor: "#FFF",shadowOffset: {width: 0,height: 0,},shadowOpacity: 0.4,shadowRadius: 10,},
        }
        Moment.locale('en');
        //NOW PLAYING
        fetch('https://api.themoviedb.org/3/movie/now_playing?api_key=1eaa7b1c6a8b5588fb8a4f12ba97ab06&language=en-US&page=1')
        .then((response) => response.json())
        .then((json) => {
            const data = JSON.stringify(json.results);
              let i = 0;
              JSON.parse(data, (key, value) => {
                //console.warn(value);
                if(key == "id"){
                    this.setState({test:value});
                }else if(key == "poster_path"){
                    this.setState({
                        test:this.state.test+"|"+value
                    });
                    DATA.push(this.state.test)
                    //console.warn(this.state.test);
                    i++;
                }
              });
              this.setState({
                  now_loading:false
                })
        })
        .catch((error) => console.error("Error :"+error))
        .finally(() => {});

        this._renderItem_loading = this._renderItem_loading.bind(this);
        this._renderItem = this._renderItem.bind(this);
        
        console.warn(DATA);
    }
    componentDidMount(){
        if(this.state.upcoming_loading === true){
            //UPCOMING
            fetch('https://api.themoviedb.org/3/movie/upcoming?api_key=1eaa7b1c6a8b5588fb8a4f12ba97ab06&language=en-US&page=1')
            .then((response) => response.json())
            .then((json) => {
                this.setState({
                    upcoming:json.results,
                    upcoming_loading:false,
                })
            })
            .catch((error) => console.error("Error :"+error))
            .finally(() => {});
        }
        if(this.state.toprated_loading === true){
            //TOP RATED
            fetch('https://api.themoviedb.org/3/movie/top_rated?api_key=1eaa7b1c6a8b5588fb8a4f12ba97ab06&language=en-US&page=1')
            .then((response) => response.json())
            .then((json) => {
                this.setState({
                    toprated:json.results,
                    toprated_loading:false,
                })
            })
            .catch((error) => console.error("Error :"+error))
            .finally(() => {});
        }
        if(this.state.popular_loading === true){
            //POPULAR
            fetch('https://api.themoviedb.org/3/movie/popular?api_key=1eaa7b1c6a8b5588fb8a4f12ba97ab06&language=en-US&page=1')
            .then((response) => response.json())
            .then((json) => {
                this.setState({
                    popular:json.results,
                    popular_loading:false,
                })
            })
            .catch((error) => console.error("Error :"+error))
            .finally(() => {});
        }
    }

    _renderItem({ item }) {
        return (
          <TouchableOpacity onPress={()=>this.details(item.split('|')[0].trim())}>
            <DropShadow style={styles.itemContainer}>
                <Image source={{uri:'https://image.tmdb.org/t/p/w500'+item.split('|')[1].trim()}} 
                style={{width:'80%',height:'90%',borderRadius:10}} />
            </DropShadow>
          </TouchableOpacity>
        );
      }
      _renderItem_loading({ item }) {
        return (
          <DropShadow style={styles.itemContainer}>
            <View style={{width:'80%',height:'90%',borderRadius:10,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                <Icon name="image" size={25} color="#FFF" />
            </View>
          </DropShadow>
        );
      }
    
    details(id){
        AsyncStorage.setItem('movies_id', id.toString());

        this.props.navigation.navigate('Detail');
    }
    render() {
        return (
            <View style={{flex: 1,backgroundColor:'#07192d',flexDirection:'column'}}>
                <StatusBar translucent backgroundColor="transparent" />
                <View style={{flex:10,flexDirection:'column'}}>
                    <View style={{flex:1,flexDirection:'row',paddingTop:5}}>
                        <View style={{flex:5,justifyContent:'center',alignItems:'center'}}>
                            <Text style={{color:'#dc2a4e',fontSize:25,fontWeight:'bold'}}>
                                S I N E A S
                            </Text>
                            <Text style={{color:'white',fontSize:9,marginTop:-5}}>
                                SINEMA ANAK BANGSA
                            </Text>
                        </View>
                    </View>
                    <View style={{flex:8,flexDirection:'column'}}>
                        <ScrollView>
                            <View style={{alignItems:'center'}}>
                                <Text style={{color:'white',fontWeight:'bold'}}>N O W   P L A Y I N G</Text>
                                {this.state.now_loading === true ?(
                                    <Carousel
                                        ref={(b) => this.carousel = b}
                                        data={DATA0}
                                        renderItem={this._renderItem_loading}
                                        sliderWidth={SLIDER_WIDTH}
                                        itemWidth={ITEM_WIDTH}
                                        containerCustomStyle={styles.carouselContainer}
                                        inactiveSlideShift={0}
                                        loop={true}
                                        autoplay={true}
                                        autoplayDelay={500}
                                        autoplayInterval={3000}
                                        onSnapToItem={(index) => this.setState({ index0 })}
                                        scrollInterpolator={scrollInterpolator}
                                        slideInterpolatedStyle={animatedStyles}
                                        useScrollView={true}          
                                    />
                                ):(
                                    <Carousel
                                        ref={(c) => this.carousel = c}
                                        data={DATA}
                                        renderItem={this._renderItem}
                                        sliderWidth={SLIDER_WIDTH}
                                        itemWidth={ITEM_WIDTH}
                                        containerCustomStyle={styles.carouselContainer}
                                        inactiveSlideShift={0}
                                        loop={true}
                                        autoplay={true}
                                        autoplayDelay={500}
                                        autoplayInterval={3000}
                                        onSnapToItem={(index) => this.setState({ index })}
                                        scrollInterpolator={scrollInterpolator}
                                        slideInterpolatedStyle={animatedStyles}
                                        useScrollView={true}          
                                    />
                                )}
                            </View>
                            <View style={{paddingHorizontal:15}}>
                            <View style={{marginTop:10,}}>
                                    <Text style={{color:'white',fontWeight:'bold'}}>COMING SOON</Text>
                                </View>
                                <View style={{marginTop:10,flexDirection:'row',marginBottom:10}}>
                                    {this.state.upcoming_loading === true ?(
                                        <ScrollView horizontal>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                        </ScrollView>
                                    ):(
                                        <ScrollView horizontal>
                                            <FlatList
                                            numColumns={100}
                                            data = {this.state.upcoming}
                                            renderItem={({item})=>(
                                                <TouchableOpacity onPress={()=>this.details(item.id)}
                                                style={{marginRight:5,width:100}}>
                                                    <Image source={{uri:'https://image.tmdb.org/t/p/w500'+item.poster_path}} 
                                                    style={{width:100,height:150,borderRadius:5}} />
                                                    <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{item.title}</Text>
                                                    <Text style={{color:'#CCC',fontSize:11}}>{Moment(item.release_date).format('YYYY')}</Text>
                                                </TouchableOpacity>
                                            )}
                                            />
                                        </ScrollView>
                                    )}
                                        
                                </View>
                                <View style={{marginTop:10,}}>
                                    <Text style={{color:'white',fontWeight:'bold'}}>TOP RATED MOVIES</Text>
                                </View>
                                <View style={{marginTop:10,flexDirection:'row',marginBottom:10}}>
                                    {this.state.toprated_loading === true ?(
                                        <ScrollView horizontal>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                        </ScrollView>
                                    ):(
                                        <ScrollView horizontal>
                                            <FlatList
                                            numColumns={100}
                                            data = {this.state.toprated}
                                            renderItem={({item})=>(
                                                <TouchableOpacity onPress={()=>this.details(item.id)}
                                                style={{marginRight:5,width:100}}>
                                                    <Image source={{uri:'https://image.tmdb.org/t/p/w500'+item.poster_path}} 
                                                    style={{width:100,height:150,borderRadius:5}} />
                                                    <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{item.title}</Text>
                                                    <Text style={{color:'#CCC',fontSize:11}}>{Moment(item.release_date).format('YYYY')}</Text>
                                                </TouchableOpacity>
                                            )}
                                            />
                                        </ScrollView>
                                    )}
                                </View>
                                <View style={{marginTop:10}}>
                                    <Text style={{color:'white',fontWeight:'bold'}}>POPULAR MOVIES</Text>
                                </View>
                                <View style={{marginTop:10,flexDirection:'row',marginBottom:10}}>
                                    {this.state.popular_loading === true ?(
                                        <ScrollView horizontal>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                            <View style={{marginRight:5}}>
                                                <View style={{width:100,height:150,borderRadius:5,justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'#EEE'}} >
                                                    <Icon name="image" size={25} color="#FFF" />
                                                </View>
                                                <Text style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5}}>Loading ...</Text>
                                                <Text style={{color:'#CCC',fontSize:11}}>Loading ...</Text>
                                            </View>
                                        </ScrollView>
                                    ):(
                                        <ScrollView horizontal>
                                            <FlatList
                                            numColumns={100}
                                            data = {this.state.popular}
                                            renderItem={({item})=>(
                                                <TouchableOpacity onPress={()=>this.details(item.id)}
                                                style={{marginRight:5,width:100}}>
                                                    <Image source={{uri:'https://image.tmdb.org/t/p/w500'+item.poster_path}} 
                                                    style={{width:100,height:150,borderRadius:5}} />
                                                    <Text numberOfLines={2} style={{color:'#EEE',fontWeight:'bold',fontSize:12,marginTop:5,flexWrap:'wrap'}}>{item.title}</Text>
                                                    <Text style={{color:'#CCC',fontSize:11}}>{Moment(item.release_date).format('YYYY')}</Text>
                                                </TouchableOpacity>
                                            )}
                                            />
                                        </ScrollView>
                                    )}
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <View style={{flex:1,flexDirection:'row',borderTopWidth:0.3,borderTopColor:'#CCC'}}>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="video-camera" size={25} color="#dc2a4e" />
                            <Text style={{color:'#dc2a4e',fontSize:9}}>
                                Movies
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Tv')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="tv" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                TV Shows
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Bookmark')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="bookmark" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                Watch list
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('People')}
                        style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Icon name="user" size={25} color="#FFF" />
                            <Text style={{color:'white',fontSize:9}}>
                                People
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    carouselContainer: {
    //   marginTop: 50
    },
    itemContainer: {
      width: ITEM_WIDTH,
      height: ITEM_HEIGHT,
      alignItems: 'center',
      justifyContent: 'center',
      shadowColor: "#FFF",
      shadowOffset: {width: 0,height: 0,},
      shadowOpacity: 0.4,
      shadowRadius: 10
    },
    itemLabel: {
      color: 'white',
      fontSize: 24
    },
    counter: {
      marginTop: 25,
      fontSize: 30,
      fontWeight: 'bold',
      textAlign: 'center'
    }
  });
export default Home;