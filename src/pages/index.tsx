import Splash from './Splash';
import Home from './Home';
import Detail from './Detail';
import People from './People';
import Detail_people from './Detail_people';
import Tv from './Tv';
import Bookmark from './Bookmark';
export {Splash,Home,Detail,People,Detail_people,Tv,Bookmark};