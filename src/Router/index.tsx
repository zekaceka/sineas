import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Splash,Home,Detail,People,Detail_people,Tv,Bookmark} from '../pages';

const Stack = createStackNavigator();

const Router = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Spalsh" component={Splash} options={{headerShown:false,}} />
            <Stack.Screen name="Home" component={Home} options={{headerShown:false,}} />
            <Stack.Screen name="Detail" component={Detail} options={{headerShown:false,}} />
            <Stack.Screen name="People" component={People} options={{headerShown:false,}} />
            <Stack.Screen name="Detail_people" component={Detail_people} options={{headerShown:false,}} />
            <Stack.Screen name="Tv" component={Tv} options={{headerShown:false,}} />
            <Stack.Screen name="Bookmark" component={Bookmark} options={{headerShown:false,}} />
        </Stack.Navigator>
    )
}

export default Router;
